﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UniRx;

namespace Models
{
    public class Counter : MonoBehaviour
    {
        public IntReactiveProperty count = new IntReactiveProperty(0);
        
        public static Counter Instance;
        void Awake()
        {
            Instance = this;
        }
    }
}