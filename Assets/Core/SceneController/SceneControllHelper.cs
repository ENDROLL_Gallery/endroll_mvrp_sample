﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using System.Linq;

public class SceneControllHelper : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        if (!Enumerable.Range(0, SceneManager.sceneCount).Any(i => SceneManager.GetSceneAt(i).name == SceneName.Core))
        {
            SceneManager.LoadScene(SceneName.Core, LoadSceneMode.Additive);
        }
    }
}
