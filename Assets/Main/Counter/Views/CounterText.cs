﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class CounterText : MonoBehaviour
{
    public void setCount(int count)
    {
        GetComponent<Text>().text = count.ToString();
    }
}
