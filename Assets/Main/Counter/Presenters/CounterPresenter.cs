﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;

public class CounterPresenter : MonoBehaviour
{
    [SerializeField] Button plusButton;
    [SerializeField] Button minusButton;
    // [SerializeField] Button minusButton;
    [SerializeField] CounterText counterText;

    // Start is called before the first frame update
    void Start()
    {
        Models.Counter counter = Models.Counter.Instance;

        // view -> model
        plusButton.OnClickAsObservable().Subscribe(_ => counter.count.Value++);
        minusButton.OnClickAsObservable().Subscribe(_ => counter.count.Value--);

        // model -> view
        counter.count.AsObservable().Subscribe(counterText.setCount);
    }
}
